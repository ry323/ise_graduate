import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Sidebar from './components/Sidebar';
import Deadline from './components/admissions/Deadline';
import TuitionFees from './components/admissions/TuitionFees';
import DegreePrograms from './components/admissions/DegreePrograms';
import FAQ from './components/admissions/FAQ';
import RequirementProcess from './components/admissions/RequirementProcess';
import Checklist from './components/admissions/Checklist';
import FinancialSupport from './components/admissions/FinancailSupport';
import MSIE from './components/programs/master/MSIE';
import MSSE from './components/programs/master/MSSE';
import OnlineMSIE from './components/programs/online/OnlineMSIE';
import MSFE from './components/programs/master/MSFE';
import MSHT from './components/programs/master/MSHT';
import OnlineNonDegree from './components/programs/online/OnlineNonDegree';
import OnlineNonDegreeAA from './components/programs/online/OnlineNonDegreeAA';
import StudentResource from './components/student-life/StudentResource';
import CareerService from './components/student-life/CareerService';
import Dissertaions from './components/programs/phd/Dissertations';
import EmploymentStat from './components/past-student/EmploymentStat';
import Requirements from './components/programs/phd/Requirements';
import Milestones from './components/programs/phd/Milestones';
import StudentNews from './components/student-life/StudentNews';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div className="sidebar-container">
            <Sidebar />
          </div>
          <Switch>
            <div className="content-container">
              <Route path="/ise_graduate/admissions/tuition_fees">
                <TuitionFees />
              </Route>
              <Route path="/ise_graduate/admissions/programs">
                <DegreePrograms />
              </Route>
              <Route path="/ise_graduate/admissions/requirements_process">
                <RequirementProcess />
              </Route>
              <Route path="/ise_graduate/admissions/checklist">
                <Checklist />
              </Route>
              <Route path="/ise_graduate/admissions/financial_support">
                <FinancialSupport />
              </Route>
              <Route path="/ise_graduate/admissions/faq">
                <FAQ />
              </Route>
              <Route path="/ise_graduate/admissions/deadline">
                <Deadline />
              </Route>
              <Route path="/ise_graduate/academics/programs/master/msie">
                <MSIE />
              </Route>
              <Route path="/ise_graduate/academics/programs/master/msfe">
                <MSFE />
              </Route>
              <Route path="/ise_graduate/academics/programs/master/msht">
                <MSHT />
              </Route>
              {/* <Route path="/ise_graduate/academics/programs/master/msie-aa">
                <MSIEAA />
              </Route> */}
              <Route path="/ise_graduate/academics/programs/master/msse">
                <MSSE />
              </Route>
              <Route path="/ise_graduate/academics/programs/online/online-msie">
                <OnlineMSIE />
              </Route>
              <Route path="/ise_graduate/academics/programs/online/non-degree">
                <OnlineNonDegree />
              </Route>
              <Route path="/ise_graduate/academics/programs/online/non-degree-aa">
                <OnlineNonDegreeAA />
              </Route>
              <Route path="/ise_graduate/studentlife/student-resource">
                <StudentResource />
              </Route>
              <Route path="/ise_graduate/studentlife/student-news">
                <StudentNews />
              </Route>
              <Route path="/ise_graduate/studentlife/career-service">
                <CareerService />
              </Route>
              <Route path="/ise_graduate/paststudent/employment-stat">
                <EmploymentStat />
              </Route>
              <Route path="/ise_graduate/academics/programs/phd/dissertations">
                <Dissertaions />
              </Route>
              <Route path="/ise_graduate/academics/programs/phd/requirements">
                <Requirements />
              </Route>

              <Route path="/ise_graduate/academics/programs/phd/milestones">
                <Milestones />
              </Route>
            </div>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
