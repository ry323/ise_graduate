import React, { Component } from 'react';
import { Element, Link } from 'react-scroll';

export default class EmploymentStat extends Component {
  render() {
    return (
      <div>
        <h1>Employment Statistics</h1>
        <p>
          ISE graduate students have found many job opportunities in various well-know industries
          over the years. A summary of the last three years of placement is below. For additional
          information, please contact the ISE department. Top Salaries MS and PhD Graduates
        </p>
        <ul>
          <li>
            <Link
              to="employment-stat-salarise"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Top Salaries
            </Link>
          </li>
          <li>
            <Link
              to="employment-stat-work"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Where do they work?
            </Link>
          </li>
        </ul>
        <Element name="employment-stat-salarise" id="employment-stat-salarise">
          <h3>Top Salaries</h3>
          <p>TODO: salary table</p>
        </Element>

        <Element name="employment-stat-work" id="employment-stat-work">
          <h3>Where do they work?</h3>
          <p>TODO: table</p>
        </Element>
      </div>
    );
  }
}
