import React, { Component } from 'react';
import {
  ProSidebar,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
  Menu,
  MenuItem,
  SubMenu,
} from 'react-pro-sidebar';
import { Link } from 'react-router-dom';

import 'react-pro-sidebar/dist/css/styles.css';

class Sidebar extends Component {
  render() {
    return (
      <ProSidebar>
        <SidebarHeader>SIDE BAR</SidebarHeader>
        <SidebarContent>
          <Menu iconShape="square">
            <SubMenu title="Admissions">
              <MenuItem>
                <a href="https://grad.illinois.edu/admissions/apply">Apply Now</a>
              </MenuItem>
              <MenuItem>
                Degree and Programs
                <Link to="/ise_graduate/admissions/programs" />
              </MenuItem>
              <MenuItem>
                Requirements and Application Process
                <Link to="/ise_graduate/admissions/requirements_process" />
              </MenuItem>
              <MenuItem>
                Application Deadline
                <Link to="/ise_graduate/admissions/deadline" />
              </MenuItem>
              <MenuItem>
                <a href="https://my.ise.illinois.edu/gradstat/login.asp"> Application Status</a>
              </MenuItem>
              <MenuItem>
                Application Checklist
                <Link to="/ise_graduate/admissions/checklist" />
              </MenuItem>
              <MenuItem>
                Tuition and Fees
                <Link to="/ise_graduate/admissions/tuition_fees" />
              </MenuItem>
              <MenuItem>
                Financial Support
                <Link to="/ise_graduate/admissions/financial_support" />
              </MenuItem>
              <MenuItem>
                FAQ
                <Link to="/ise_graduate/admissions/faq" />
              </MenuItem>
            </SubMenu>
            <SubMenu title="Academics">
              <SubMenu title="Programs">
                <SubMenu title="MS Programs">
                  <MenuItem>
                    MSSE
                    <Link to="/ise_graduate/academics/programs/master/msse" />
                  </MenuItem>
                  <MenuItem>
                    MSIE
                    <Link to="/ise_graduate/academics/programs/master/msie" />
                  </MenuItem>
                  <MenuItem>
                    <a href="https://msfe.illinois.edu/">MSFE</a>
                  </MenuItem>
                  <MenuItem>
                    <a href="https://ahs.illinois.edu/ht-overview">MSHT</a>
                  </MenuItem>
                </SubMenu>
                <SubMenu title="Phd Programs">
                  <MenuItem>
                    Degree Requirements
                    <Link to="/ise_graduate/academics/programs/phd/requirements" />
                  </MenuItem>
                  <MenuItem>
                    Deposited Dissertations
                    <Link to="/ise_graduate/academics/programs/phd/dissertations" />
                  </MenuItem>
                  <MenuItem>
                    Major Milestones and Time Limits
                    <Link to="/ise_graduate/academics/programs/phd/milestones" />
                  </MenuItem>
                </SubMenu>
                <SubMenu title="Online Programs">
                  <MenuItem>
                    Online MSIE
                    <Link to="/ise_graduate/academics/programs/online/online-msie" />
                  </MenuItem>
                  <MenuItem>
                    Online Non-Degree Options
                    <Link to="/ise_graduate/academics/programs/online/non-degree" />
                  </MenuItem>
                  <MenuItem>
                    Online Non-Degree Certificate in Advanced Analytics
                    <Link to="/ise_graduate/academics/programs/online/non-degree-aa" />
                  </MenuItem>
                </SubMenu>
              </SubMenu>
              <MenuItem>
                <a href="https://ise.illinois.edu/courses/">Courses</a>
              </MenuItem>
            </SubMenu>
            <SubMenu title="Student Life">
              <MenuItem>
                Student Resources
                <Link to="/ise_graduate/studentlife/student-resource" />
              </MenuItem>
              <MenuItem>
                Career Services
                <Link to="/ise_graduate/studentlife/career-service" />
              </MenuItem>
              <MenuItem>
                Student News
                <Link to="/ise_graduate/studentlife/student-news" />
              </MenuItem>
            </SubMenu>
            <SubMenu title="Past Students">
              <MenuItem>
                Employment Statistics
                <Link to="/ise_graduate/paststudent/employment-stat" />
              </MenuItem>
            </SubMenu>
          </Menu>
        </SidebarContent>
        <SidebarFooter>COPY RIGHT</SidebarFooter>
      </ProSidebar>
    );
  }
}

export default Sidebar;
