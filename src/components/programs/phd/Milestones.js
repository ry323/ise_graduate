import React, { Component } from 'react';
import { Element, Link } from 'react-scroll';
import pic from './Milestones.jpg';

export default class Milestones extends Component {
  render() {
    return (
      <div>
        <h1>Degree Milestones and Time Limits</h1>
        <ul>
          <li>
            <Link
              to="phd-milestones"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Degree Milestones
            </Link>
          </li>
          <li>
            <Link
              to="phd-time-limits"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Time Limits
            </Link>
          </li>
        </ul>
        <Element name="phd-milestones" id="phd-milestones">
          <h3>Degreee Milestones</h3>
          <img src={pic} alt="Milestones" width="50%" height="60%" />
        </Element>
        <Element name="phd-time-limits" id="phd-time-limits">
          <h3>Time Limits</h3>
          <p>
            TODO:{' '}
            <a href="https://wiki.illinois.edu/wiki/display/ISEGRADSTUDENTS/PhD+Students">table</a>
          </p>
        </Element>
      </div>
    );
  }
}
