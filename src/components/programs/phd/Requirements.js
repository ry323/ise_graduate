import React, { Component } from 'react';
import { Element, Link } from 'react-scroll';

export default class Requirements extends Component {
  render() {
    return (
      <div>
        <h1>Degree Requirements</h1>
        <p>
          Students pursuing the PhD have the option of entering the program with a B.S. or MS
          degree. If entering with a B.S., students may earn their MS degree during the program or
          not at all. For additional information regarding our PhD programs and process, see PhD
          Degree Information.
        </p>

        <ul>
          <li>
            <Link
              to="phd-coursework-requirements"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Coursework Requirements
            </Link>
            <ul>
              <li>
                <Link
                  to="phd-ie"
                  spy={true}
                  smooth={true}
                  duration={500}
                  id="scroll-link"
                  activeClass="active"
                >
                  PhD in Industrial Engineering
                </Link>
              </li>

              <li>
                <Link
                  to="phd-se"
                  spy={true}
                  smooth={true}
                  duration={500}
                  id="scroll-link"
                  activeClass="active"
                >
                  PhD in Systems and Entrepreneurial Engineering
                </Link>
              </li>
            </ul>
          </li>
          <li>Qualifying Exam(TODO: add wiki hyperlink)</li>
          <li>Preliminary Exam(TODO: add wiki hyperlink)</li>
          <li>Final Exam or Dissertation Defense(TODO: add wiki hyperlink)</li>
          <li>Dissertation Deposit(TODO: add wiki hyperlink)</li>
        </ul>
        <Element name="phd-coursework-requirements" id="phd-coursework-requirements">
          <Element name="phd-ie" id="phd-ie">
            <h3>PhD in Industrial Engineering</h3>
            <h4>Entering with approved MS/MA degree</h4>
            <ul>
              <li>IE 599 Thesis Research (min-max applied toward the degree)—32 hours</li>
              <li>
                IE 590 Seminar (registration for 0 hours every term while in residence)—0 hours
              </li>
              <li>
                Elective courses – chosen in consultation with advisor—32 hours (subject to Other
                Requirements and Conditions below)
              </li>

              <li>Total Hours 64</li>
            </ul>
            <h5>Other Requirements and Conditions (may overlap)</h5>
            <ul>
              <li>Minimum 500-level Hours Required Overall—16 hours</li>
              <li>
                A maximum of 4 hours of IE 597 (or other approved independent study) may be applied
                toward the elective course work requirement. This course does not count toward the
                IE 500-level requirement.
              </li>
              <li>
                4 hours of the elective courses must be from a College of Engineering department,
                including ABE and CHBE.
              </li>
              <li>
                4 hours of the elective courses must be from a College of Engineering department,
                including ABE and CHBE. A maximum of 4 CR-graded credit hours in non-IE courses may
                be applied toward the degree.
              </li>
              <li>Minimum GPA: 3.0</li>
            </ul>
            <h4>Entering with approved BS/BA degree </h4>
            <ul>
              <li>IE 599 Thesis Research (min-max applied toward the degree)—40 hours</li>
              <li>
                IE 590 Seminar (registration for 0 hours every term while in residence)—0 hours
              </li>
              <li>
                Elective courses – chosen in consultation with advisor—56 hours (subject to Other
                Requirements and Conditions below)
              </li>
              <li>Total Hours 96</li>
            </ul>
            <h5>Other Requirements and Conditions (may overlap)</h5>
            <ul>
              <li>Minimum 500-level Hours Required Overall—24 hours</li>
              <li>
                A maximum of 4 hours of IE 597 (or other approved independent study) may be applied
                toward the elective course work requirement. This course does not count toward the
                IE 500-level requirement.
              </li>
              <li>
                4 hours of the elective courses must be from a College of Engineering department,
                including ABE and CHBE.
              </li>
              <li>
                4 hours of the elective courses must be from a College of Engineering department,
                including ABE and CHBE. A maximum of 4 CR-graded credit hours in non-IE courses may
                be applied toward the degree.
              </li>
              <li>Minimum GPA: 3.0</li>
            </ul>
          </Element>

          <Element name="phd-se" id="phd-se">
            <h3>PhD in Systems and Entrepreneurial Engineering</h3>
            <h4>Entering with approved MS/MA degree</h4>
            <ul>
              <li>SE 599 Thesis Research (min-max applied toward the degree)—32 hours</li>
              <li>
                SE 590 Seminar (registration for 0 hours every term while in residence)—0 hours
              </li>
              <li>
                Approved SE and IE courses—16 hours Elective courses – chosen in consultation with
                advisor—16 hours (subject to Other Requirements and Conditions below)
              </li>
              <li>Total Hours—64</li>
            </ul>
            <h5>Other Requirements and Conditions (may overlap)</h5>
            <ul>
              <li>
                Minimum 500-level credit hours applied toward the degree, all of which must come
                from a College of Engineering department, including ABE and CHBE.—16 hours
              </li>
              <li>
                The Elective courses must be at the 500- level and from a College of Engineering
                department including ABE and CHBE.
              </li>
              <li>
                A maximum of 8 hours of SE 597 (or other approved independent study) may be applied
                toward the elective course work requirement. This course does not count toward the
                500-level requirement.
              </li>
              <li>
                At least 64 hours of credit, which may include SE 599, must be earned in residence.
              </li>
            </ul>

            <h4>Entering with approved BS/BA degree </h4>
            <ul>
              <li>Master's degree equivalent—32 hours</li>
              <li>SE 599 Thesis Research (min-max applied toward the degree)—32 hours</li>
              <li>
                SE 590 Seminar (registration for 0 hours every term while in residence)—0 hours
              </li>
              <li>Approved SE and IE courses—16 hours</li>
              <li>
                Elective courses – chosen in consultation with advisor—16 hours (subject to Other
                Requirements and Conditions below)
              </li>
              <li>Total Hours—96</li>
            </ul>

            <h5>Other Requirements and Conditions (may overlap)</h5>
            <ul>
              <li>
                Minimum 500-level credit hours applied toward the degree, all of which must come
                from a College of Engineering department, including ABE and CHBE.—28 hours
              </li>
              <li>
                The Elective courses must be at the 500- level and from a College of Engineering
                department including ABE and CHBE.
              </li>
              <li>
                A maximum of 8 hours of SE 597 (or other approved independent study) may be applied
                toward the elective course work requirement. This course does not count toward the
                500-level requirement.
              </li>
              <li>
                At least 64 hours of credit, which may include SE 599, must be earned in residence.
              </li>
            </ul>
          </Element>
        </Element>
      </div>
    );
  }
}
