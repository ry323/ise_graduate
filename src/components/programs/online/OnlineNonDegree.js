import React, { Component } from 'react';
import { Element, Link } from 'react-scroll';

export default class OnlineNonDegree extends Component {
  render() {
    return (
      <div>
        <h1>Online Non-Degree Options</h1>
        <ul>
          <li>
            <Link
              to="online-non-degree-eligibility"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Eligibility
            </Link>
          </li>
          <li>
            <Link
              to="online-non-degree-tuition"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Tuition Costs
            </Link>
          </li>
          <li>
            <Link
              to="online-non-degree-courses"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Courses Offerings
            </Link>
          </li>
          <li>
            <Link
              to="online-non-degree-register"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              How to Register
            </Link>
          </li>
          <li>
            <Link
              to="online-non-degree-transfer"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Transfering to Degree-Seeking
            </Link>
          </li>
          <li>
            <Link
              to="online-non-degree-aa"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Pursue the online Advanced Analytics Certificate
            </Link>
          </li>
        </ul>

        <Element name="online-non-degree-eligibility" id="online-non-degree-eligibility">
          <h3>Eligibility</h3>
          <p>
            You do not need to be admitted to a degree or certificate program in order to enroll in
            an online course. Any student with a bachelor's degree can register for an online course
            as a non-degree student (please note that non-degree students who are taking courses are
            not in a degree program and successful completion of these courses does not guarantee
            admissions into the degree program.). Individuals are responsible for determining if
            they have the required course prerequisites prior to registering.
          </p>
        </Element>

        <Element name="online-non-degree-tuition" id="online-non-degree-tuition">
          <h3>Tuition Costs</h3>
          <p>
            To learn more about tuition and payments, visit
            <a href="https://grainger.illinois.edu/academics/online/tuition-funding">here</a>
          </p>
        </Element>
        <Element name="online-non-degree-courses" id="online-non-degree-courses">
          <h3>Courses Offerings</h3>
          <p>
            The rigor, assignments, projects, and exams of online course are the same as the
            on-campus courses and same course deadlines throughout the semester without being on
            campus. It is flexibility to view course lectures when time allows during the week.
          </p>
          <p>
            To view the upcoming semester’s course offerings, visit{' '}
            <a href="https://grainger.illinois.edu/academics/online/courses">here</a>
          </p>
        </Element>
        <Element name="online-non-degree-register" id="online-non-degree-register">
          <h3>How to Register</h3>
          <p>
            Online course availability, registration deadlines, and information is available at
            http://engineering.illinois.edu/online/.
          </p>
          <ul>
            <li>
              Select one of the available courses you wish to register for.Make sure to use the CRN
              (Course Reference Number) for the course.
            </li>
            <li>
              Complete the registration steps under “How to Register”. In step 3, click on the “All
              other individuals” link, which is for non-degree students.
            </li>
            <li>
              If necessary, visit the refund deadline schedule to learn when students may drop or
              withdraw from the semeste
            </li>
          </ul>
        </Element>

        <Element name="online-non-degree-transfer" id="online-non-degree-transfer">
          <h3>Transfering to Degree-Seeking</h3>
          <p>
            There is no limit to the number of credit hours that can be taken as a non-degree
            student; however, if one then chooses to pursue a online degree, there is a limit to the
            number of credit hours that can be transferred to the online degree program. The
            following conditions apply when completing courses as a non-degree student:
          </p>
          <ul>
            <li>
              A maximum of 12 credit hours of coursework taken outside the graduate college may be
              transferred to a degree program, if the grade is a “B” or higher
            </li>
            <li> Starting as a non-degree student does not guarantee admission into the program</li>
          </ul>
        </Element>
        <Element name="online-non-degree-aa" id="online-non-degree-aa">
          <h3>Pursue the online Advanced Analytics Certificate</h3>
          <p>
            See more details{' '}
            <a href="http://localhost:3000/academics/programs/online/non-degree-aa">here</a>
          </p>
        </Element>
      </div>
    );
  }
}
