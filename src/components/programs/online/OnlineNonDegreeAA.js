import React, { Component } from 'react';
import { Element, Link } from 'react-scroll';

export default class OnlineNonDegreeAA extends Component {
  render() {
    return (
      <div>
        <h1>Online Non-Degree Certificate in Advanced Analytics</h1>
        <ul>
          <li>
            <Link
              to="online-non-degree-aa-eligibility"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Eligibility
            </Link>
          </li>

          <li>
            <Link
              to="online-non-degree-aa-requirements"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Certificate Requirements
            </Link>
          </li>

          <li>
            <Link
              to="online-non-degree-aa-tuition"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Tuition Costs
            </Link>
          </li>

          <li>
            <Link
              to="online-non-degree-aa-courses"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Courses Offerings
            </Link>
          </li>

          <li>
            <Link
              to="online-non-degree-aa-schedule"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Semester Schedule
            </Link>
          </li>

          <li>
            <Link
              to="online-non-degree-aa-register"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              How to Register
            </Link>
          </li>
        </ul>
        <Element name="online-non-degree-aa-eligibility" id="online-non-degree-aa-eligibility">
          <h3>Eligibility</h3>
          <p>
            You must hold a bachelor’s degree and have the background necessary to complete
            graduate-level courses in advanced analytics gained through either their bachelor’s
            degree or work experience. Those interested in earning this non-degree certificate must
            send an email to Kim Sgarbossa at ksgarb17@illinois.edu to sign up.
          </p>
        </Element>
        <Element name="online-non-degree-aa-requirements" id="online-non-degree-aa-requirements">
          <h3>Certificate Requirements</h3>
          <p>
            Students must complete a total of 12 credit hours with a grade of B or higher in each
            course. Students must choose both
          </p>
          <ul>
            <li>8 credit hours from the Advanced Analytics Core list</li>
            <li>
              4 credit hours from the Advanced Analytics Secondary list (or an additional four hours
              from the Core course list)
            </li>
          </ul>

          <h6>Advanced Analytics Core Course List</h6>
          <ul>
            <li>IE 528 Computing for Data Analytics—4 hours</li>
            <li>IE 529 Stats of Big Data and Clustering—4 hours</li>
            <li>IE 530 Optimization for Data Analytics—4 hours</li>
            <li>IE 531 Algorithms for Data Analytics—4 hours</li>
            <li>IE 532 Analysis of Network Data—4 hours</li>
            <li>IE 533 Big Graphs and Social Networks—4 hours</li>
          </ul>

          <h6>Advanced Analytics Secondary Course List</h6>
          <ul>
            <li>IE 400 Design & Analysis of Experiments—4 hours</li>
            <li>IE 410 Stochastic Processes & Applications—4 hours</li>
            <li>IE 411 Optimization of Large Systems—4 hours</li>
            <li>IE 510 Applied Nonlinear Programming—4 hours</li>
            <li>IE 511 Integer Programming—4 hours</li>
            <li>IE 521 Convex Optimization—4 hours</li>
          </ul>
        </Element>
        <Element name="online-non-degree-aa-tuition" id="online-non-degree-aa-tuition">
          <h3>Tuition Costs</h3>
          <p>
            To learn more about tuition and payments, visit
            <a href="https://grainger.illinois.edu/academics/online/tuition-funding">here</a>
          </p>
        </Element>
        <Element name="online-non-degree-aa-courses" id="online-non-degree-aa-courses">
          <h3>Courses Offerings</h3>
          <p>
            The rigor, assignments, projects, and exams of online course are the same as the
            on-campus courses and same course deadlines throughout the semester without being on
            campus. It is flexibility to view course lectures when time allows during the week.
          </p>
          <p>
            A subset of the Advanced Analytics courses is offered online each semester – fall,
            spring, and summer. To view the upcoming semester’s course offerings, visit{' '}
            <a href="https://grainger.illinois.edu/academics/online/courses">here</a>
          </p>
          <p>
            Questions regarding future course offerings can be directed to the College of
            Engineering Office of Graduate, Professional and Online Programs at
            engr-gpp@illinois.edu.{' '}
          </p>
        </Element>
        <Element name="online-non-degree-aa-schedule" id="online-non-degree-aa-schedule">
          <h3>Semester Schedule</h3>
          <p>
            The academic calendar is posted{' '}
            <a href="http://illinois.edu/calendar/list/4328">here</a>. This includes semester start
            and end dates, tuition refund deadlines for dropped courses, withdrawals, cancellations
            for the given semester, and much more.
          </p>
        </Element>
        <Element name="online-non-degree-aa-register" id="online-non-degree-aa-register">
          <h3>How to Register</h3>
          <ul>
            <li>
              Determine the course(s) for the upcoming term. Make sure to use the CRN (Course
              Reference Number) for the online course. See
              https://grainger.illinois.edu/academics/online for course schedule and CRNs.
            </li>
            <li>
              Complete the registration steps at
              https://grainger.illinois.edu/admissions/online-registration under “How to Register”.
              In step 3, click on the “All other individuals” link, which is for non-degree
              students/certificate students.
            </li>
            <li>
              Registration deadlines are posted at
              https://grainger.illinois.edu/admissions/online-registration.
            </li>
          </ul>
          <p>
            For assistance or questions with registration, contact Frank Hoskinson at
            fhoskins@illinois.edu.
          </p>
        </Element>
      </div>
    );
  }
}
