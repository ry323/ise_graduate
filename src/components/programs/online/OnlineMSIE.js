import React, { Component } from 'react';
import { Element, Link } from 'react-scroll';

export default class OnlineMSIE extends Component {
  render() {
    return (
      <div>
        <h1>Online-MSIE</h1>
        <p>
          The MS Industrial Engineering degree is offered online. Students pursue a non-thesis
          option in the program. Degree requirements are the same for online and on-campus students.
          Students in the non-thesis option will complete 32 hours of coursework and 4 hours of
          independent study, for a total of 36 hours. Students are also eligible to complete the
          Advanced Analytics Concentration as a part of their degree, which is a transcriptable
          concentration.
        </p>
        <ul>
          <li>
            <Link
              to="online-msie-degree-requierements"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Degree Requirements
            </Link>
          </li>
          <li>
            <Link
              to="online-msie-aa"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Advanced Analytics Concentration
            </Link>
          </li>
        </ul>

        <Element name="online-msie-degree-requirements" id="online-msie-degree-requirements">
          <h3>Degree Requirements</h3>
          <ul>
            <li>
              IE 590 Seminar (registration for 0 hours every term while in residence)* — 0 hours
            </li>
            <li>IE 597 Independent Study—4 hours</li>
            <li>
              Elective courses chosen in consultation with advisor (subject to Other Requirements
              and Conditions below)—32 hours
            </li>
            <li>Total—36 hours</li>
          </ul>

          <h5>Other Requirements and Conditions (may overlap)</h5>
          <ul>
            <li>
              A minimum of 12 500-level credit hours applied toward the degree, 8 of which must be
              IE.
            </li>
            <li>
              The non-thesis option is for students terminating their studies with the MS degree.
            </li>
            <li>
              For students in the non-thesis option, 4 hours of IE 597 are required (4 hours maximum
              allowed towards the MS degree), because each student must show evidence of the ability
              to do independent research. This course does not count toward the IE 500-level
              requirement
            </li>
            <li>3.0 Minimum GPA</li>
          </ul>
        </Element>

        <Element name="online-msie-aa" id="online-msie-aa">
          <h4>Advanced Analytics Concentration</h4>
          <p>TODO : Intro to AA concentration</p>
          <p>
            To earn the degree with advanced analytics concentration, you need to satisfy extra
            requirements and conditions listed below:
          </p>
          <ul>
            <li>Advanced Analytics Concentration *—12 hours</li>
            <ul>
              <li>Advanced Analytics Core—8 hours</li>
              <li>Advanced Analytics Secondary—4 hours (or 4 additional hours from Core list)</li>
            </ul>
            <li>Must earn a “B” or better in all Advanced Analytics courses.</li>
          </ul>

          <h6>Advanced Analytics Core Course List</h6>
          <ul>
            <li>IE 528 Computing for Data Analytics—4 hours</li>
            <li>IE 529 Stats of Big Data and Clustering—4 hours</li>
            <li>IE 530 Optimization for Data Analytics—4 hours</li>
            <li>IE 531 Algorithms for Data Analytics—4 hours</li>
            <li>IE 532 Analysis of Network Data—4 hours</li>
            <li>IE 533 Big Graphs and Social Networks—4 hours</li>
          </ul>

          <h6>Advanced Analytics Secondary Course List</h6>
          <ul>
            <li>IE 400 Design & Analysis of Experiments—4 hours</li>
            <li>IE 410 Stochastic Processes & Applications—4 hours</li>
            <li>IE 411 Optimization of Large Systems—4 hours</li>
            <li>IE 510 Applied Nonlinear Programming—4 hours</li>
            <li>IE 511 Integer Programming—4 hours</li>
            <li>IE 521 Convex Optimization—4 hours</li>
          </ul>
        </Element>
      </div>
    );
  }
}
