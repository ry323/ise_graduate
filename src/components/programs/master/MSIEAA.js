import React, { Component } from 'react';
import { Element, Link } from 'react-scroll';

export default class MSIEAA extends Component {
  render() {
    return (
      <div>
        <h1>
          MS in Industrial Engineering MS in Industrial Engineering with the Advanced Analytics
          Concentration{' '}
        </h1>
        <p>
          The MS in Industrial Engineering degree with the Advanced Analytics Concentration is
          offered on-campus and online. Students may choose to pursue the thesis or non-thesis
          option in the program. As a general guideline, students who do not wish to pursue a PhD
          program should enroll in the non-thesis option; students who plan to pursue a PhD must
          complete the thesis option.
        </p>
        <p>
          Degree requirements are the same for online and on-campus students. Students pursuing the
          thesis option will complete 24 hours of coursework and 8 hours of thesis research, for a
          total of 32 hours. Students pursuing the non-thesis option will complete 32 hours of
          coursework and 4 hours of independent study, for a total of 36 hours.
        </p>

        <h3>Degree Requirements</h3>
        <h4>Concentration in Advanced Analytics Course List</h4>
        <h4>Thesis Option </h4>
        <h4>Non-thesis Option</h4>
      </div>
    );
  }
}
