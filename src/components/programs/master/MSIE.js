import React, { Component } from 'react';
import { Element, Link } from 'react-scroll';

export default class MSIE extends Component {
  render() {
    return (
      <div>
        <h1> MS in Industrial Engineering</h1>
        <p>
          The MS Industrial Engineering degree is offered on-campus and online. Student may choose
          to pursue the thesis or non-thesis option in the program. As a general guideline, students
          who do not wish to pursue a PhD program should enroll in the non-thesis option; students
          who plan to pursue a PhD must complete the thesis option. Degree requirements are the same
          for online and on-campus students. Students pursuing the thesis option will complete 24
          hours of coursework and 8 hours of thesis research, for a total of 32 hours. Students
          pursuing the non-thesis option will complete 32 hours of coursework and 4 hours of
          independent study, for a total of 36 hours.
        </p>

        <ul>
          <li>
            {' '}
            <Link
              to="msie-degree-requirements"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Degree Requirements
            </Link>
          </li>
          <ul>
            <li>
              <Link
                to="msie-thesis"
                spy={true}
                smooth={true}
                duration={500}
                id="scroll-link"
                activeClass="active"
              >
                Thesis Option
              </Link>
            </li>
            <li>
              <Link
                to="msie-non-thesis"
                spy={true}
                smooth={true}
                duration={500}
                id="scroll-link"
                activeClass="active"
              >
                Non-thesis Option
              </Link>
            </li>
            <li>
              <Link
                to="msie-aa"
                spy={true}
                smooth={true}
                duration={500}
                id="scroll-link"
                activeClass="active"
              >
                Advanced Analytics Concentration
              </Link>
            </li>
          </ul>
          <li>
            <Link
              to="msie-degree-planning"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Degree Planning
            </Link>
          </li>
        </ul>

        <Element name="msie-degree-requirements" id="msie-degree-requirements">
          <h3>Degree Requirements</h3>
          <Element name="msie-thesis" id="msie-thesis">
            <h4>Thesis Option </h4>
            <ul>
              <li>IE 599 Thesis Research (min-max applied toward the degree) — 8 hours</li>
              <li>
                IE 590 Seminar (registration for 0 hours every term while in residence)*—0 hours
              </li>
              <li>
                Elective courses chosen in consultation with advisor—24 hours (subject to Other
                Requirements and Conditions below)
              </li>
              <li>32 Total Hours</li>
            </ul>
            <h5>Other Requirements and Conditions (may overlap)</h5>
            <ul>
              <li>
                A minimum of 12 500-level credit hours applied toward the degree, 8 of which must be
                IE.
              </li>
              <li>
                A maximum of 4 hours of IE 597 (or other approved independent study) may be applied
                toward the elective course work requirement. This course does not count toward the
                IE 500-level requirement.
              </li>
              <li>3.0 Minimum GPA</li>
            </ul>
          </Element>
          <Element name="msie-non-thesis" id="msie-non-thesis">
            <h4>Non-thesis Option</h4>
            <ul>
              <li>
                IE 590 Seminar (registration for 0 hours every term while in residence)* — 0 hours
              </li>
              <li>IE 597 Independent Study—4 hours</li>
              <li>
                Elective courses chosen in consultation with advisor (subject to Other Requirements
                and Conditions below)—32 hours
              </li>
              <li>Total Hours—36</li>
            </ul>
            <h5>Other Requirements and Conditions (may overlap)</h5>
            <ul>
              <li>
                A minimum of 12 500-level credit hours applied toward the degree, 8 of which must be
                IE.
              </li>
              <li>
                The non-thesis option is for students terminating their studies with the MS degree.
              </li>
              <li>
                For students in the non-thesis option, 4 hours of IE 597 are required (4 hours
                maximum allowed towards the MS degree), because each student must show evidence of
                the ability to do independent research. This course does not count toward the IE
                500-level requirement 3.0
              </li>
              <li>Minimum GPA *IE 590</li>
            </ul>
          </Element>
          <Element name="msie-aa" id="msie-aa">
            <h4>Advanced Analytics Concentration</h4>
            <p>TODO : Intro to AA concentration</p>
            <p>
              To earn the degree with advanced analytics concentration, you need to satisfy extra
              requirements and conditions listed below:
            </p>
            <ul>
              <li>Advanced Analytics Concentration *—12 hours</li>
              <ul>
                <li>Advanced Analytics Core—8 hours</li>
                <li>Advanced Analytics Secondary—4 hours (or 4 additional hours from Core list)</li>
              </ul>
              <li>Must earn a “B” or better in all Advanced Analytics courses.</li>
            </ul>
            <h6>Advanced Analytics Core Course List</h6>
            <ul>
              <li>IE 528 Computing for Data Analytics—4 hours</li>
              <li>IE 529 Stats of Big Data and Clustering—4 hours</li>
              <li>IE 530 Optimization for Data Analytics—4 hours</li>
              <li>IE 531 Algorithms for Data Analytics—4 hours</li>
              <li>IE 532 Analysis of Network Data—4 hours</li>
              <li>IE 533 Big Graphs and Social Networks—4 hours</li>
            </ul>
            <h6>Advanced Analytics Secondary Course List</h6>
            <ul>
              <li>IE 400 Design & Analysis of Experiments—4 hours</li>
              <li>IE 410 Stochastic Processes & Applications—4 hours</li>
              <li>IE 411 Optimization of Large Systems—4 hours</li>
              <li>IE 510 Applied Nonlinear Programming—4 hours</li>
              <li>IE 511 Integer Programming—4 hours</li>
              <li>IE 521 Convex Optimization—4 hours</li>
            </ul>
          </Element>
        </Element>

        <Element name="msie-degree-planning" id="msie-degree-planning">
          <h3>Degree Plannning</h3>
          <p>
            The degree requirements in ISE for our M.S. degrees are very flexible. This is so that
            the student can pursue courses in his/her research area without constraint. To assist
            the student, ISE has created several sample curricula, sorted by research area. The
            student may change/substitute courses as needed to fulfill the needs of his/her career
            goals.
          </p>
          <ul>
            <li>
              <a href="https://ise.illinois.edu/graduate/sample-curricula/Data%20Analytics%20NT.pdf">
                Data Analytics
              </a>
            </li>
            <li>
              <a href="https://ise.illinois.edu/graduate/sample-curricula/or-analyst.pdf">
                Operations Research-Analyst
              </a>
            </li>
            <li>
              <a href="https://ise.illinois.edu/graduate/sample-curricula/OR-Network%20Optimization.pdf">
                Operations Research-Network Optimization
              </a>
            </li>
            <li>
              <a href="https://ise.illinois.edu/graduate/sample-curricula/or-optimization.pdf">
                Operations Research-Optimization
              </a>
            </li>
            <li>
              <a href="https://ise.illinois.edu/graduate/sample-curricula/OR-supply%20chain.pdf.pdf">
                Operations Research-Supply Chain
              </a>
            </li>
          </ul>
        </Element>
      </div>
    );
  }
}
