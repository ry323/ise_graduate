import React, { Component } from 'react';
import { Element, Link } from 'react-scroll';

export default class MSSE extends Component {
  render() {
    return (
      <div>
        <div>
          <h1> MS in Systems and Entrepreneurial Engineering</h1>
          <ul>
            <li>
              {' '}
              <Link
                to="msse-degree-requirements"
                spy={true}
                smooth={true}
                duration={500}
                id="scroll-link"
                activeClass="active"
              >
                Degree Requirements
              </Link>
            </li>
            <ul>
              <li>
                <Link
                  to="msse-thesis"
                  spy={true}
                  smooth={true}
                  duration={500}
                  id="scroll-link"
                  activeClass="active"
                >
                  Thesis Option
                </Link>
              </li>
              <li>
                <Link
                  to="msse-non-thesis"
                  spy={true}
                  smooth={true}
                  duration={500}
                  id="scroll-link"
                  activeClass="active"
                >
                  Non-thesis Option
                </Link>
              </li>
            </ul>

            <li>
              <Link
                to="msse-degree-planning"
                spy={true}
                smooth={true}
                duration={500}
                id="scroll-link"
                activeClass="active"
              >
                Degree Planning
              </Link>
            </li>
          </ul>

          <p>
            Students in the MS Systems and Entrepreneurial Engineering degree program may choose to
            pursue the thesis or non-thesis option in the program. As a general guideline, students
            who do not wish to pursue a PhD program should enroll in the non-thesis option; students
            who plan to pursue a PhD must complete the thesis option. Students pursuing the thesis
            option will complete 28 hours of coursework and 4 hours of thesis research, for a total
            of 32 hours. Students pursuing the non-thesis option will complete 28 hours of
            coursework and 8 hours of project design, for a total of 36 hours.
          </p>
          <Element name="msse-degree-requirements" id="msse-degree-requirements">
            <h3>Degree Requirements</h3>
            <Element name="msse-thesis" id="msse-thesis">
              <h4>Thesis Option</h4>
              <ul>
                <li>SE 599 Thesis Research (min-max applied toward the degree)—4 hours</li>
                <li>
                  SE 590 Seminar (registration for 0 hours every term while in residence)—0 hours
                </li>
                <li>SE courses at the 500-level—12 hours</li>
                <li>
                  Elective courses – chosen in consultation with advisor—16 hours(subject to Other
                  Requirements and Conditions below)
                </li>

                <li>Total Hours—32</li>
              </ul>
              <h5>Other Requirements and Conditions (may overlap)</h5>
              <ul>
                <li>
                  For the thesis option, a maximum of 4 hours of SE 597 (or other approved
                  independent study) may be applied toward the elective course work requirement.
                  This course does not count toward the SE 500-level requirement.
                </li>
                <li>
                  4 hours of the elective courses must be from a College of Engineering department,
                  including ABE and CHBE.
                </li>
                <li>
                  A maximum of 4 CR-graded credit hours in non-SE courses may be applied toward the
                  degree.
                </li>
                <li>Minimum program GPA: 3.25</li>
              </ul>
            </Element>
            <Element name="msse-non-thesis" id="msse-non-thesis">
              <h4>Non-thesis Option</h4>
              <ul>
                <li>
                  SE 590 Seminar (registration for 0 hours every term while in residence)—0 hours
                </li>
                <li>SE 594 Project Design—8 hours</li>

                <li>SE courses at the 500-level—12 hours</li>
                <li>
                  Elective courses – chosen in consultation with advisor—16 hours (subject to Other
                  Requirements and Conditions below)
                </li>
                <li>Total Hours—36</li>
              </ul>
              <h5>Other Requirements and Conditions (may overlap)</h5>
              <ul>
                <li>
                  4 hours of the elective courses must be from a College of Engineering department,
                  including ABE and CHBE.
                </li>
                <li>
                  A maximum of 4 CR-graded credit hours in non-SE courses may be applied toward the
                  degree.
                </li>
                <li>Minimum program GPA: 3.25</li>
              </ul>
            </Element>
          </Element>
          <Element name="msse-degree-planning" id="msse-degree-planning">
            <h3>Degree Planning</h3>
            <p>
              The degree requirements in ISE for our M.S. degrees are very flexible. This is so that
              the student can pursue courses in his/her research area without constraint. To assist
              the student, ISE has created several sample curricula, sorted by research area. The
              student may change/substitute courses as needed to fulfill the needs of his/her career
              goals.
            </p>
            <ul>
              <li>
                <a href="https://ise.illinois.edu/graduate/sample-curricula/Controls%20(Non-thesis)%20Example%201.pdf">
                  Decision and Control - 1
                </a>
              </li>
              <li>
                <a href="https://ise.illinois.edu/graduate/sample-curricula/Controls%20(Non-thesis)%20Example%202.pdf">
                  Decision and Control - 2
                </a>
              </li>
              <li>
                <a href="https://ise.illinois.edu/graduate/sample-curricula/Controls%20(Non-thesis)%20Example%203.pdf">
                  Decision and Control - 3
                </a>
              </li>
              <li>
                <a href="https://ise.illinois.edu/graduate/sample-curricula/design-and-manufacturing.pdf">
                  Design and Manufacturing
                </a>
              </li>
            </ul>
          </Element>
        </div>
      </div>
    );
  }
}
