import React, { Component } from 'react';

export default class TuitionFees extends Component {
  render() {
    return (
      <div>
        <h1>Tuition and Fees</h1>
        <h3>Phd Students</h3>
        <p>
          Students in our PhD program are fully funded through the The Grainger College of
          Engineering’s Five-Year PhD Funding Guarantee during their first five years of enrollment.
          Starting in Fall 2020, ISE PhD students are guaranteed a funded appointment for fall and
          spring that includes a full tuition waiver, a partial fee waiver, and a stipend. Students
          are also eligible for summer support. Students must remain in Good Academic Standing and
          successfully perform the duties of their assistantships.
        </p>

        <h3>On-Campus Master Students</h3>
        <p>
          Tuition and Fees will vary depending on your residency status, the degree you have been
          admitted to, and the number of credit hours you take per semester – charged as a “range”.
          The University provides a{' '}
          <a href="https://cost.illinois.edu/Home/Cost">Cost Calculator</a> so you can get an idea
          of what you can expect to pay per semester or academic year.
        </p>

        <h3>Online Students</h3>
        <p>
          You can find current tuition rates on The Grainger College of Engineering’s{' '}
          <a href="https://cost.illinois.edu/Homehttps://grainger.illinois.edu/academics/online/tuition-funding/Cost">
            online tuition rates page.
          </a>
        </p>
      </div>
    );
  }
}
