import React, { Component } from 'react';

export default class DegreePrograms extends Component {
  render() {
    return (
      <div>
        <h1>Degree and Programs</h1>

        <h3>MS Programs</h3>
        <ul>
          <li>
            <a href="/ise_graduate/academics/programs/master/msie">MS Industrial Engineering</a>
          </li>
          <li>
            <a href="/ise_graduate/academics/programs/master/msse">
              MS Systems and Entrepreneurial Engineering
            </a>
          </li>
        </ul>
        <h3>Phd Programs</h3>
        <ul>
          <li>
            <a href="/ise_graduate/academics/programs/phd/requirements#phd-ie">
              PhD in Industrial Engineering
            </a>
          </li>

          <li>
            <a href="/ise_graduate/academics/programs/phd/requirements#phd-se">
              PhD in Systems and Entrepreneurial Engineering
            </a>
          </li>
        </ul>
        <h3>Online Programs</h3>
        <ul>
          <li>
            <a href="/ise_graduate/academics/programs/online/online-msie">
              Online MS Industrial Engineering
            </a>
          </li>
          <li>
            <a href="/ise_graduate/academics/programs/online/non-degree">
              Online Non-Degree Options
            </a>
          </li>

          <li>
            <a href="/ise_graduate/academics/programs/online/non-degree-aa">
              Online Non-Degree Certificate in Advanced Analytics
            </a>
          </li>
        </ul>
      </div>
    );
  }
}
