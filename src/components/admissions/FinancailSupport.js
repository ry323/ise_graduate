import React, { Component } from 'react';

export default class FinancailSupport extends Component {
  render() {
    return (
      <div>
        <h1>Financail Support</h1>
        <h3>ISE Graduate Student Funding</h3>
        <p>
          While the number of applicants to our programs exceeds the amount of funding available, we
          do offer several opportunities for students to gain funding:
        </p>
        <ul>
          <li> Fellowships and Awards</li>

          <li>Teaching Assistantships</li>
          <li>Research Assistantships</li>
        </ul>
        <p>
          At the University level, there are additional sources of funding. You can check out more
          information on each of the following links:
        </p>
        <ul>
          <li>Graduate Assistantship Clearinghouse </li>
          <li> Graduate College Fellowships Office</li>
          <li>Campus Job Boards </li>
        </ul>
        <p>
          Note that the Master of Science in Financial Engineering is a self-funded program where
          students are responsible for paying their tuition and fees. Students in this program are
          not eligible for Board of Trustee (BOT) tuition-waiver-generating assistantships at the
          University of Illinois.
        </p>
      </div>
    );
  }
}
