import React, { Component } from 'react';

export default class RequirementProcess extends Component {
  render() {
    return (
      <div>
        <h1>Requirement and Procedure</h1>
        <h3>Eligibility</h3>
        <p>
          Applicants must have a bachelor’s degree in an engineering discipline (mathematics
          acceptable) from a regionally accredited institution in the United States or a comparable
          degree from a recognized institution of higher learning abroad to apply to the MS or Ph.D.
          program. Students do not have to have a Master’s degree to apply to the Ph.D. program.
          Applicants who already hold a master’s degree may not apply for a second master’s degree
          in the same field.
        </p>

        <h3>Application Process</h3>
        <p>TODO : step-by-step application process</p>

        <p>
          Applicants should not apply to more than one ISE program per semester. All application
          materials may be uploaded online during the application process. Please DO NOT send
          official copies of transcripts, diplomas, reference letters, or scores. Applicants will be
          contacted if additional materials are required. Please visit each page to view the
          application requirements, depending on your status:
        </p>

        <ul>
          <li>Domestic Applicants</li>
          <li>International Applicants</li>
          <li>Current University of Illinois ISE Undergraduates</li>
        </ul>

        <p>
          Full-time employees of the University of Illinois will be admitted only as on-campus
          students and may complete this program on a part-time basis.
        </p>
      </div>
    );
  }
}
