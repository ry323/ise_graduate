import React, { Component } from 'react';
import { Element, Link } from 'react-scroll';

export default class Checklist extends Component {
  render() {
    return (
      <div>
        <h1>Application Checklist</h1>
        <ul>
          <li>
            <Link
              to="checklist-domestic"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Domestic Students
            </Link>
          </li>
          <li>
            <Link
              to="checklist-international"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              International Students
            </Link>
          </li>
          <li>
            <Link
              to="checklist-current-undergraduate"
              spy={true}
              smooth={true}
              duration={500}
              id="scroll-link"
              activeClass="active"
            >
              Current University of Illinois ISE Undergraduates
            </Link>
          </li>
        </ul>
        <Element name="checklist-domestic" id="checklist-domestic">
          <h3>Domestic Students</h3>
          <ul>
            <li>Online application</li>
            <li>Application fee of $70</li>
            <li>Statement of Purpose</li>
            <li>Resume</li>
            <li>Three letters of recommendation</li>
            <li>
              Transcripts (may be unofficial, but applicant name and university name must appear)
            </li>
            <li>
              Official GRE scores (explore the GRE General Test at Home as a potential option
              through September 30, 2020)
            </li>
          </ul>
        </Element>
        <Element name="checklist-international" id="checklist-international">
          <h3>International Students</h3>
          <ul>
            <li>Online application</li>
            <li>Application fee of $90</li>
            <li>Statement of purpose</li>
            <li>Resume</li>
            <li>Three letters of recommendation</li>
            <li>
              Transcripts in English and native language (may be unofficial, but applicant name and
              university name must appear)
            </li>
            <li>Certificate of degree in English and native language (if earned)</li>
            <li>
              Official GRE scores (explore the GRE General Test at Home as a potential option
              through September 30, 2020)
            </li>
            <li>
              Official TOEFL scores* (explore the TOEFL iBT® Special Home Edition as a potential
              option through September 30, 2020
            </li>
            <li>Evidence of financial resources*</li>
            <li>
              Passport/Visa — a copy of your passport and current visa, if applicable, must be
              uploaded to the application. If you do not have a passport, you will be required to
              have one before the visa eligibility documents can be sent, if admitted. Please note
              that a visa is not required of students completing our online masters degree.
            </li>
          </ul>
        </Element>
        <Element name="checklist-current-undergraduate" id="checklist-current-undergraduate">
          <h3>Current University of Illinois ISE Undergraduates</h3>
          <ul>
            <li>
              Online application (fee will be paid by ISE; contact ise-grad@illinois.edu after
              submission)
            </li>
            <li>DARS Report (wait for fall or spring grades to post; name must be on report)</li>
            <li>Statement of Purpose</li>
            <li>Resume</li>
            <li>One letter of recommendation from an ISE faculty member</li>
            <li>Passport (international only)</li>
            <li>Evidence of financial resources (international students only*)</li>
          </ul>
        </Element>
      </div>
    );
  }
}
